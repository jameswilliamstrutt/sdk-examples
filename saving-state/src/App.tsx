import './App.css'
import Editor from '@monaco-editor/react';
import { useGiraffeState } from '@gi-nx/iframe-sdk-react';
import { rpc, giraffeState } from '@gi-nx/iframe-sdk';
import { useEffect, useRef } from 'react';

const setData = changes => {
  // every "app" has access to an unstructured JSON store called "public"
  // note: it's "public" to other apps the user has added to that Giraffe project
  const activePa = giraffeState.get('selectedProjectApp');
  rpc.invoke('updateProjectApp', [
    activePa.app,
    {
      ...activePa,
      public: {
        ...activePa.public,
        ...changes
      }
    }
  ]);
};


function App() {
  const data = useGiraffeState("selectedProjectApp")?.public;
  const editorRef = useRef(null);
  
  function handleEditorDidMount(editor) {
    editorRef.current = editor;
  }

  useEffect(() => {
    if (!editorRef.current) return;
    // @ts-ignore
    editorRef.current.setValue(JSON.stringify((data || {}), undefined, 2));
  }, [data]);

  return (
    <>
      <Editor
        height="50vh"
        defaultLanguage="JSON"
        onMount={handleEditorDidMount}
      />
      <button onClick={() => {
        setData(JSON.parse(editorRef.current.getValue() || "{}"));
      }}>Save value</button>
    </>
  )
}

export default App
