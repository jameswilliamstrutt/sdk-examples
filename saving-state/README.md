# syncing state
A simple example showing persisting state in a Giraffe project 

Read:
```ts
const data = useGiraffeState("selectedProjectApp")?.public;
```

Write:
```ts
const setData = changes => {
  // every "app" has access to an unstructured JSON store called "public"
  // note: it's "public" to other apps the user has added to that Giraffe project
  const activePa = giraffeState.get('selectedProjectApp');
  rpc.invoke('updateProjectApp', [
    activePa.app,
    {
      ...activePa,
      public: {
        ...activePa.public,
        ...changes
      }
    }
  ]);
};
```
