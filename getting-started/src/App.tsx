import "./App.css";

import { useEffect } from "react";
import transformRotate from "@turf/transform-rotate";

import { useGiraffeState } from "@gi-nx/iframe-sdk-react";
import { rpc, giraffeState } from "@gi-nx/iframe-sdk";


function App() {

  // listen to changes in giraffe geometries (bakedSections), rotate them, then write back a temporary layer with the contents
  const bakedSections = useGiraffeState("bakedSections");
  useEffect(() => {
    const rotated = {
      type: "FeatureCollection",
      features: [],
    };
    for (let deg = 0; deg < 360; deg += 30) {
      if (bakedSections.features.length > 0)
        rotated.features.push(
          // @ts-ignore
          ...(transformRotate(bakedSections, deg).features as Feature[])
        );
    }
    const layerName = "rotated";
    rpc.invoke("updateTempLayerGeoJSON", [layerName, rotated]);
  
  }, [bakedSections]);

  return (
    <div className="App">
      <div>
        Giraffe section count is: {bakedSections.features.length}
      </div>
    </div>
  );
}

export default App;
