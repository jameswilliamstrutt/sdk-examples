# Vite + React

This is a [Vite](https://vitejs.dev) project together with React.
# Iframe SDK - Getting Started Example App

Here’s a simple example that demonstrates getting data from Giraffe into your app and back again.

You can run this code locally or you can go ahead and fork this example on Codesandbox [here](https://codesandbox.io/p/sandbox/icy-https-xtmx83?file=%2Fsrc%2FApp.tsx%3A1%2C1). The url to put in Giraffe can be grabbed from the right hand side

![screenshot](screenshot.png)

We started with the basic React + Vite sandbox “cloud” example. Then installed the Giraffe sdk and one lib we’re going to use `yarn add @gi-nx/iframe-sdk-react @turf/transform-rotate` Below are the changes we made to App.tsx with some comments

```tsx
...
// **import our libraries**
import transformRotate from "@turf/transform-rotate";
import { useGiraffeState } from "@gi-nx/iframe-sdk-react";
import { rpc } from "@gi-nx/iframe-sdk";
...

function App() {
  **// hook to use derived Giraffe geometry live **
  const bakedSections = useGiraffeState("bakedSections");

  useEffect(() => {

		**// the "app", creates a new layer by rotating the Giraffe geometry by 0, 30, 60, 90 ... degrees**
    const rotated = {
      type: "FeatureCollection",
      features: [],
    };
    for (let deg = 0; deg < 360; deg += 30) {
      if (bakedSections.features.length > 0)
        rotated.features.push(
          ...(transformRotate(bakedSections, deg).features as Feature[])
        );
    }
    const layerName = "rotated";
		
		**// send the data back to Giraffe as a layer (will be visible in left hand bar and on the map)** 
    rpc.invoke("updateTempLayerGeoJSON", [layerName, rotated]);
  }, [bakedSections]);

  return (
    ...
    <div>
				**// render some data from Giraffe**
        Giraffe section count is {bakedSections.features.length}
        ...
    </div>
  );
}
...
```