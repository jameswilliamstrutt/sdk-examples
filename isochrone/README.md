# An Isochrone app for Giraffe using the Iframe SDK 

This app ports the [Mapbox example](https://docs.mapbox.com/help/tutorials/get-started-isochrone-api/) to run in the Giraffe SDK.

It is built from React + Vite and deployed on Netflify following [this tutorial](https://docs.netlify.com/integrations/frameworks/vite/).

You can add this app to a Giraffe project by searching 'isochrone' in the app menu (+ on right).
If you run this locally and make changes, you can see the results by setting a project to look at a local URL instead: ☰ main menu -> Advanced -> JSON editor -> select 'isochrone' -> edit `private.url`.

## Building the app

[https://www.loom.com/share/4feb33335d6c41c18f6ebead80a06022?sid=e389bff2-bc49-41fa-968f-ff8006317b32](https://www.loom.com/share/4feb33335d6c41c18f6ebead80a06022?sid=e389bff2-bc49-41fa-968f-ff8006317b32)

## Adding the “official” version to a project

This is the code from the above repo -  deployed to Netlify.

[https://www.loom.com/share/483c0349779c4faaa7be7f713e64b45d?sid=4d7ce0bc-620e-416e-a435-a10ae59ea242](https://www.loom.com/share/483c0349779c4faaa7be7f713e64b45d?sid=4d7ce0bc-620e-416e-a435-a10ae59ea242)


### Setup

Create an app, I called it `isochrone`
```
npm create vite@latest
```

Install dependencies
```
yarn
yarn add @gi-nx/iframe-sdk-react
yarn add -D netlify-cli
```

Set the env variable
```
export VITE_MAPBOX_TOKEN=<get one from https://www.mapbox.com/>
```

Dev with 
```
yarn run dev
```

Build and deploy the app
```
yarn build
yarn run netlify deploy --prod --dir dist
```
