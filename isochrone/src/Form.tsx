
// A React port from https://docs.mapbox.com/help/tutorials/get-started-isochrone-api/

const MAPBOX_TOKEN = import.meta.env.VITE_MAPBOX_TOKEN;
// Create constants to use in getIso()
const urlBase = 'https://api.mapbox.com/isochrone/v1/mapbox/';
// Create a function that sets up the Isochrone API query then makes an fetch call

export type Profile = 'walking' | 'cycling' | 'driving'

export async function getIso([lon, lat]: [number, number], profile: Profile, minutes: number) {
  const query = await fetch(
    `${urlBase}${profile}/${lon},${lat}?contours_minutes=${minutes}&polygons=true&access_token=${MAPBOX_TOKEN}`,
    { method: 'GET' }
  );
  const data = await query.json();
  return data;
  console.log(data);
}

export default function Form({ profile, setProfile, minutes, setMinutes }: {
  profile: Profile, minutes: string, setProfile: (profile: Profile) => void, setMinutes: (minutes: string) => void
}) {

  return <div className='my24 mx24 py24 px24 bg-gray-faint round'>
    <form id='params'
      onChange={(event) => {
        // @ts-ignore
        if (event.target.name === 'profile') setProfile(event.target.value);
        // @ts-ignore
        else if (event.target.name === 'minutes') setMinutes(event.target.value);
      }}
    >
      <h4 className='txt-m txt-bold mb6'>Choose a travel mode:</h4>
      <div className='mb12 mr12 toggle-group align-center'>
        <label className='toggle-container'>
          <input name='profile' type='radio' value='walking' checked={profile === 'walking'} />
          <div className='toggle toggle--active-null toggle--null'>Walking</div>
        </label>
        <label className='toggle-container'>
          <input name='profile' type='radio' value='cycling' checked={profile === 'cycling'} />
          <div className='toggle toggle--active-null toggle--null'>Cycling</div>
        </label>
        <label className='toggle-container'>
          <input name='profile' type='radio' value='driving' checked={profile === 'driving'} />
          <div className='toggle toggle--active-null toggle--null'>Driving</div>
        </label>
      </div>
      <h4 className='txt-m txt-bold mb6'>Choose a maximum minutes:</h4>
      <div className='mb12 mr12 toggle-group align-center'>
        <input name='minutes' type='number' min="0" step="5" value={minutes} onChange={e => setMinutes(e.target.value)}/>
        {/* <label className='toggle-container'>
          <input name='minutes' type='radio' value='10' checked={minutes === '10'} />
          <div className='toggle toggle--active-null toggle--null'>10 min</div>
        </label>
        <label className='toggle-container'>
          <input name='minutes' type='radio' value='20' checked={minutes === '20'} />
          <div className='toggle toggle--active-null toggle--null'>20 min</div>
        </label>
        <label className='toggle-container'>
          <input name='minutes' type='radio' value='30' checked={minutes === '30'} />
          <div className='toggle toggle--active-null toggle--null'>30 min</div>
        </label> */}
      </div>
    </form>
  </div>
}