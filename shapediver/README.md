This project was setup with a [React + TypeScript + Vite template](https://vitejs.dev/guide/).
The commands to set it up were
```
npm create vite@latest
# then follow prompts, `shapediver` as name
cd shapediver
npm install
# giraffe deps
npm install @gi-nx/iframe-sdk @gi-nx/iframe-sdk-react
# shapediver deps
npm install @shapediver/sdk.geometry-api-sdk-v2
# run locally
npm run dev
```

To use it in Giraffe, add the `iFrame` app.



