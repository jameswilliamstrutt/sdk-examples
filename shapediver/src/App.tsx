import { useRef, useState } from 'react';
import { giraffeState, rpc } from '@gi-nx/iframe-sdk';

import { transform } from './transform';
import './App.css';

async function callShapeDiver(params) {
  // get data from giraffe
  const projectOrigin = giraffeState.get('projectOrigin');
  // get all the geometry
  // const rawSections = giraffeState.get('rawSections');
  // or just the selected geometries
  const rawSections = await rpc.invoke('getSelectedFeatures', []);

  const projectedrawSections = await rpc.invoke('toProjected', [rawSections, projectOrigin]);
  // TODO edit transform to use ShapeDiver API - the function should receive and provide GeoJSON in local "meters" projection
  const transformed = await transform(projectedrawSections, projectOrigin, params);
  const unprojected = await rpc.invoke('fromProjected', [transformed, projectOrigin]);

  // send data to giraffe

  // OPTION 1: update the sections on the map
  unprojected.features.forEach(f => {
    rpc.invoke('updateRawSection', [f]);
  });

  // if you get bakedSections which is the transformed geometry you probably don't want to write it back using updateRawSection but could instead try the below

  // OPTION 2: this is a "temporary" output that won't be saved - if you want to update something frequently and refresh each time they open the app
  // note: it's a "data layer" so users cannot edit the geometry and rendering is customized via mapbox or lens style
  // rpc.invoke('updateTempLayerGeoJSON', ['temp output', unprojected]);
}

function App() {
  const [loading, setLoading] = useState(false);
  const params = useRef({
    ticket:
      'd5aea971ab29326fc7d6374a493722b97cd75330863f523a06baab3d22d02ce5edf5ed4a8d92b5aab950a3fcfdc57b61c4341209949b8031ecb64064cb1574b6822c1b6fb982d834623017656d53213be39d066624d0f9f1f0f64cb40045bf79d044f1e9c82afc-e26e41656c062c9f1f312e92f305a5b0',
    url: 'https://sdr7euc1.eu-central-1.shapediver.com'
  });

  return (
    <div className="container">
      {/* <h2>
        ShapeDiver / Giraffe Example
      </h2> */}
      {/* https://www.shapediver.com/favicon.ico */}
      <img
        style={{ width: '100%' }}
        src="https://camo.githubusercontent.com/4bc9d81490017c2e988d6c73452d5ca21a55cc8120738fc96c75a8931aa5fb5b/68747470733a2f2f7364757365312d6173736574732e736861706564697665722e636f6d2f70726f64756374696f6e2f6173736574732f696d672f6e61766261725f6c6f676f2e706e67"
        alt="ShapeDiver Logo"
      />

      <ol>
        <li>Select a geometry on the map</li>
        <li>Click "Transform" to run ShapeDiver</li>
      </ol>

      <div className="row">
        <span>ShapeDiver URL: </span>
        <input
          defaultValue={params.current.url}
          onChange={e => (params.current.url = e.target.value)}
        />
      </div>

      <div className="row">
        <span>ShapeDiver Ticket: </span>
        <input
          defaultValue={params.current.ticket}
          onChange={e => (params.current.ticket = e.target.value)}
        />
      </div>

      <div className="button">
        <button
          onClick={() => {
            if (loading) return;
            setLoading(true);
            callShapeDiver(params.current).then(() => setLoading(false));
          }}
          disabled={loading}
        >
          {loading ? 'loading...' : 'Transform'}
        </button>
      </div>

    </div>
  );
}

export default App;
